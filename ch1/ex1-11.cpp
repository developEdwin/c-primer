/*
	
	Print numbers in between the input numbers, excluding the input numbers.

*/

#include "iostream"

using namespace std;

int main()
{
	int num1 = 0, num2 = 0;

	cout << "Enter two numbers: " << endl;
	cin >> num1 >> num2;

	while (num2 > num1)
	{
		num1++;

		// 'If' conditional to break loop before printing boundary number
		if(num1 >= num2)
			break;
		
		cout << num1 << endl;
	}

	return 0;
}