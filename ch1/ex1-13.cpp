#include "iostream"

using namespace std;

int main()
{
	int small = 0, big = 0;

	cout << "Enter two numbers: " << endl;
	cin >> small >> big;

	// Accepts any input order
	if (big < small)
		swap(small, big);

	for (int i = small; i != big; ++i)
		cout << i << endl;

	return 0;
}