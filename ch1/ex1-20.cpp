#include <iostream>
#include "Sales_item.h"

int main()
{
    Sales_item trans;

    std::cout << "To terminate program, press ctrl + d then enter" << std::endl;

    while (std::cin >> trans)
        std::cout << trans << std::endl;

    return 0;
}
