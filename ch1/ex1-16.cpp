/*

	Program to read input until end-of-file.

*/

#include "iostream"

using namespace std;

int main()
{
	int value = 0, sum = 0;

	cout << "Introduce numbers to sum up." << endl;

	cout << "Hit enter then ctrl+d to finish writing." <<
		endl;

	while (cin >> value)
		sum += value;

	cout << "The sum is: " << sum << endl;

	return 0;
}