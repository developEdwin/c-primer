#include "iostream"

using namespace std;

int main()
{
	int count = 10;

	cout << "Countdown from C++!" << endl;

	while (count >= 0)
	{
		cout << count << endl;
		--count;
	}

	return 0;
}