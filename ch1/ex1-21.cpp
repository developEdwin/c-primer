#include <iostream>
#include "Sales_item.h"

using namespace std;

int main()
{
    Sales_item item1, item2;

    cout << "To terminate program, press Ctrl + D then enter." << endl;

    while (cin >> item1 >> item2)
    {
        if (item1.isbn() == item2.isbn())
            cout << item1 + item2 << endl;
        else
            cerr << "Not the same ISBN." << endl;
    }

    return 0;

}
