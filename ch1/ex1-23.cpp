#include <iostream>
#include "Sales_item.h"

using namespace std;

int main()
{
    Sales_item currTrans, trans;

    if (cin >> currTrans)
    {
        int cnt = 1;
        while (cin >> trans)
        {
            if (trans.isbn() == currTrans.isbn())
                ++cnt;

            else
            {
                cout << currTrans << " occurs " << cnt << " times" << endl;
                currTrans = trans;
                cnt = 1;
            }
        }

        cout << currTrans << " occurs " << cnt << " times" << endl;
    }

    return 0;
}
