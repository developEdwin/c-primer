#include <iostream>
#include "Sales_item.h"

using namespace std;

int main()
{
    Sales_item item, currItem;

    if (cin >> currItem)
    {
        while (cin >> item)
        {
            if (currItem.isbn() == item.isbn())
                currItem += item;

            else
            {
                cout << currItem << endl;
                currItem = item;
            }
        }

        cout << currItem << endl;
    }

    else
    {
        cerr << "No input." << endl;
        return -1;
    }

    return 0;
}
